//Write a function that accepts a string as a parameter.
//Conditions:
//1. It can only accept up to 15 characters.
//2. If the number of characters is even, return the string in reverse.
//3. If the number of characters is odd, return the string in alphabetical order.


fun main(args: Array<String>){
    do {
        println("Please enter a string/word:")
        val inputString = readLine()!!

        if(inputString.length > 15)
            println("Please enter 15 characters only.")
        else
            println(isEven(inputString))
    }while(inputString.length > 15)
}

fun isEven(inputString: String): String {
    return if(inputString.length % 2 == 0)
        "The number of characters is even, therefore, the reverse of $inputString is ${inputString.reversed()}"
    else {
        "The number of characters is odd, therefore, the alphabetical order of $inputString is " +
                "${inputString.toCharArray().sorted().joinToString("")}"
    }
}
