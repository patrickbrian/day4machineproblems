//Write an application that allows a user to enter the names and birthdates of up to 10
//friends. Continue to prompt the user for names and birthdates until the user enters
//the sentinel value ZZZ for a name or has entered 10 names, whichever comes first. When
//the user is finished entering names, produce a count of how many names were entered,
//and then display the names. In a loop, continuously ask the user to type one of the
//names and display the corresponding birthdate or an error message if the name has not
//been previously entered. The loop continues until the user enters ZZZ for a name.

fun main(args: Array<String>){
    val records = mutableMapOf<String, String>()
    var name = ""
    var birthdate = ""
    var index = 0
    do{
        print("Please enter a name: ")
        name = readLine()!!
        if(name != "ZZZ") {
            print("Please enter the birthdate: ")

            birthdate = readLine()!!

            records.put(name.lowercase(), birthdate)
            index++

            println()
        }
    }while(index < 10 && name != "ZZZ")

    println("\nYou entered ${records.size} names:")
    records.forEach{ record -> println("${record.key.first().uppercaseChar() + 
            record.key.substring(1).lowercase()}")}

    do{
        print("\nPlease enter a name to display its birthdate: ")
        name = readLine()!!

        if(name != "ZZZ") {
            if (records.containsKey(name.lowercase())) println("Birthdate: ${records.getValue(name.lowercase())}")
            else println("The name you've entered was not in the record.")
        }

    }while(name != "ZZZ")



}