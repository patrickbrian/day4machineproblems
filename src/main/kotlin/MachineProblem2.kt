//Write a function that accepts a list as a parameter.
//Conditions:
//a. The list contains string values of the colors of the rainbow. (roygbiv).
//b. Some of the colors can have duplicates in the list.
//c. Count the occurrence of each color, and return a list of mapped values.
//d. If the value of the string, is not in the colors of the rainbow, it will be counted under "Others"
//e. The case of the string does not matter, meaning it should be able to process both uppercase, lowercase, and mix case.
//Ex:
//Sample Input: listOf("red", "blue", "blue", "pink")
//Expected Output: { red = 1, blue = 2, others = 1}

fun main(args: Array<String>){
    val inputColors = listOf("red", "blue", "blue", "pink")
    println(countRainbowColors(inputColors))
}

fun countRainbowColors(inputColors: List<String>): MutableMap<String, Int>{
    val colorCount = mutableMapOf<String, Int>()
    inputColors.forEach{ color ->
        if(color.lowercase() == "red") checkMapValue(colorCount, "red")
        else if(color.lowercase() == "orange") checkMapValue(colorCount, "orange")
        else if(color.lowercase() == "yellow") checkMapValue(colorCount, "yellow")
        else if(color.lowercase() == "green") checkMapValue(colorCount, "green")
        else if(color.lowercase() == "blue") checkMapValue(colorCount, "blue")
        else if(color.lowercase() == "indigo") checkMapValue(colorCount, "indigo")
        else if(color.lowercase() == "violet") checkMapValue(colorCount, "violet")
        else checkMapValue(colorCount, "others")
    }
    return colorCount
}

fun checkMapValue(colorCount: MutableMap<String, Int>, color: String){
    if(colorCount.containsKey(color))
        colorCount.replace(color, colorCount.getValue(color)+1)
    else
        colorCount.put(color, 1)
}
