//A personal phone directory contains room for first names and phone numbers for 30 people.
//Assign names and phone numbers for the first 10 people. Prompt the user for a name,
//and if the name is found in the list, display the corresponding phone number. If the name
//is not found in the list, prompt the user for a phone number, and add the new name and
//phone number to the list. Continue to prompt the user for names until the user enters quit.
//After the lists are full (containing 30 names), do not allow the user to add new entries.

fun main(args: Array<String>){
    val phoneDirectory = mutableListOf(
        listOf("Patrick", "09111111111"),
        listOf("Andrea", "09222222222"),
        listOf("Brian", "09333333333"),
        listOf("Nicole", "09444444444"),
        listOf("Kate", "09555555555"),
        listOf("Josh", "09666666666"),
        listOf("John", "09777777777"),
        listOf("Philip", "09888888888"),
        listOf("Russell", "09999999999"),
        listOf("Ben", "09123456789"),
    )

    do {
        println("Please enter a name: ")
        val inputName = readLine()!!
        if(inputName.lowercase() != "quit") {

            //checking if the name has a phone number
            val details = phoneDirectory.filter { personalInfo ->
                personalInfo[0].lowercase() == inputName.lowercase()
            }

            //if a record was retrieved, print the capitalized name and phone number
            if (!details.isNullOrEmpty()) println(
                "${inputName.first().uppercaseChar() + inputName.substring(1).lowercase()} number " +
                        "was found in the record. The phone number " + "is ${details[0][1]}."
            )
            else {
                //else ask for the phone number
                println("No phone number retrieved. Please enter your phone number: ")
                do {
                    val newPhoneNumber = readLine()!!

                    //check if the input phone number contains a letter
                    if (!newPhoneNumber.all { Character.isDigit(it) })
                        println("Phone number must only be numbers.")
                    else {

                        //if the input phone number is valid, add the capitalized name and phone number in the record
                        phoneDirectory.add(
                            listOf(
                                inputName.first().uppercaseChar() +
                                        inputName.substring(1).lowercase(), newPhoneNumber
                            )
                        )
                        println("Record successfully added!\n")
                    }
                } while (!newPhoneNumber.all { Character.isDigit(it) })
            }
        }
    }while(inputName.lowercase() != "quit" && phoneDirectory.size <= 30)
}