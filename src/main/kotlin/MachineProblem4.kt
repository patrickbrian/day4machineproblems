//Write an application that contains an array of 10 multiple-choice quiz questions
//related to your favorite hobby. Each question contains three answer choices.
//Also create an array that holds the correct answer to each question—A, B, or C.
//Display each question and verify that the user enters only A, B, or C as the
//answer—if not, keep prompting the user until a valid response is entered.
//If the user responds to a question correctly, display Correct!; otherwise,
//display The correct answer is ... and the letter of the correct answer. After
//the user answers all the questions, display the number of correct and incorrect answers.

fun main(args: Array<String>){
    val questions = mapOf(
        "1" to mapOf(
            "question" to "Zharvakko is the name of which hero, also known for placing wards and stunning foes " +
                "with a ranged cask?", "a" to "Venomancer", "b" to "Oracle", "c" to "Voljin"),
        "2" to mapOf("question" to "Which of the following heroes is actually a walrus?",
            "a" to "Kunkka", "b" to "Rylai", "c" to "Tusk"),
        "3" to mapOf("question" to "\"Beep boop!\" What ranged wispy Strength hero is able to relocate " +
                "allies on the map?", "a" to "Shendelzare", "b" to "Kaldr", "c" to "Io"),
        "4" to mapOf("question" to "Although an Agility hero, what character's strong attacks often make him able to " +
                "defeat Roshan in early stages of the game?", "a" to "Bristleback", "b" to "Ursa", "c" to "Barathrum"),
        "5" to mapOf("question" to "Squee, Spleen, and Spoon use mines to damage enemies. What is their " +
                "collective name?", "a" to "Techies", "b" to "Tinker", "c" to "Timber"),
        "6" to mapOf("question" to "Which melee hero is able to go invisible, but only within her own spun " +
                "webs?", "a" to "Eredar", "b" to "Razor", "c" to "Broodmother"),
        "7" to mapOf("question" to "Which hero, known for garnering a huge bounty in the game, can share Aghanim's " +
                "Scepter with other characters?", "a" to "Alchemist", "b" to "Puck", "c" to "Pugna"),
        "8" to mapOf("question" to "Which Intelligence-based hero's Hex spell can transform a foe into a chicken?",
            "a" to "Lion", "b" to "Rhasta", "c" to "Rubick"),
        "9" to mapOf("question" to "Which hero's ultimate attack allows him to create clones of himself, even though " +
                "one's death kills them all?", "a" to "Medusa", "b" to "Meepo", "c" to "Azwraith"),
        "10" to mapOf("question" to "What ranged hero's ability to assassinate a foe can take out a damaged enemy " +
                "hero with one well-placed shot?", "a" to "Alleria", "b" to "Kardel", "c" to "Traxex")
    )

    val answers = mapOf(
        "1" to "c",
        "2" to "c",
        "3" to "c",
        "4" to "b",
        "5" to "a",
        "6" to "c",
        "7" to "a",
        "8" to "b",
        "9" to "b",
        "10" to "b",
    )

    var userAnswer = ""
    var correct = 0
    var incorrect = 0

    questions.forEach { question ->
        println("${question.key}. ${question.value.getValue("question")}")
        println("\ta. ${question.value.getValue("a")}")
        println("\tb. ${question.value.getValue("b")}")
        println("\tc. ${question.value.getValue("c")}")

        do {
            print("\nPlease type the letter of your answer: ")
            userAnswer = readLine()!!.lowercase()
            if(userAnswer != "a" && userAnswer != "b" && userAnswer != "c")
                println("You answer is not valid. Please enter letter A,B or C only.")
        }while(userAnswer != "a" && userAnswer != "b" && userAnswer != "c")

        if(!answers.filter{ answer -> question.key == answer.key && userAnswer == answer.value }.isNullOrEmpty())
        {
            println("Correct!\n")
            correct++
        }
        else {
            println("The correct answer is ${answers.filter{ answer -> question.key == answer.key}.values.first()}.\n")
            incorrect++
        }
    }

    println("\nYou have $correct correct answers, and $incorrect incorrect answers.")
}